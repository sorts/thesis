% -------------------------------------------------------------
% Universidad de Alicante  
% -------------------------------------------------------------
% PROYECTO	: Phd Thesis Sergio Orts Escolano
% ARCHIVO	: 06_conclusions.tex 
% AUTOR		: Sergio Orts Escolano [sorts]		
% FECHA		: 21/03/2013
% -------------------------------------------------------------
% Conclusions and future work chapter  
% -------------------------------------------------------------

\chapter{Conclusions}
\label{cap:conclusiones}

\shadowbox{
\begin{minipage}{0.9\textwidth}
	This chapter discusses the main conclusions extracted from the work presented in this dissertation. The chapter is organized in four different sections: Section \ref{cap:conclusions:conclusions} presents and discusses the final conclusions of the work presented in this thesis. Section \ref{cap:conclusions:contributions} enumerates the most relevant contributions made in the topic of research. Next, Section \ref{cap:conclusions:publications} lists my publications derived from the presented work. Finally, Section \ref{cap:conclusions:future} presents future works: open problems and research topics that remain for future research. 
\end{minipage}
}

\section{Conclusions}
\label{cap:conclusions:conclusions}

In this thesis, we first presented a computing method based on Growing Self-Organizing Maps (GSOM) to create 3D models from unorganized raw noisy 3D data. The proposed method does not need information about the kind of sensor used to obtain data so the method works with most 3D sensors. Moreover, the capacity of GSOMs to represent noisy 3D data distributions was demonstrated. Topology preservation capabilities of GSOMs were studied and compared with other methods, showing several advantages for 3D data representation. 

The Growing Neural Gas algorithm (GNG) is considered one of the most convenient methods to solve the presented problem as it has some beneficial attributes: flexibility, rapid adaptation, topology preservation, noise removal, dimensionality reduction, etc. We demonstrated the validity of our proposal testing our method with several models and performed a study of the network parameterization by calculating the quality of representation and also comparing results with other methods like Voxel Grid (VG). We also demonstrated that the proposed method obtained better adaptation to the input space than other filtering methods, obtaining lower error on simulated scenes and CAD models with different levels of noise.

In addition, we modified the original GNG algorithm to represent 3D data sequences, which accelerated the learning algorithm and allowed the method to work faster. This is possible because the algorithm does not restart the map for each new acquisition, but instead readjusts the network structure starting from a previously generated representation and without inserting or deleting neurons. Moreover, we improved original method considering colour information. Neurons' reference vectors were modified including colour components so during the learning stage network structure not only adapt to the input space topology but also learns colour information from the input space. This avoids the necessity to add post-processing steps to include colour information in the final representation. Finally, Competitive Hebbian Learning (CHL) process was extended allowing the GNG algorithm to generate faces during the learning stage and therefore producing a polygonal mesh representation of the input space. 

Furthermore, we demonstrated the capabilities of the GNG algorithm to improve keypoint detection process in computer vision applications. Noisy 3D data captured using low cost sensors like the Kinect device was filtered and downsampled using the GNG algorithm. The GNG network provided a reduced and compact 3D structure which has less information than the original data, but keeping the topology. It was demonstrated that state-of-the-art keypoint detection algorithms performed better on filtered point clouds using the proposed method. Moreover, the efficiency of the method was improved since less iterations were required to reject false matchings and to improve its precision. The proposed method was validated in a 3D scene registration process, obtaining lower transformation errors in most detector-descriptor combinations that used the GNG representation as an input data. The most accurate transformations between different point clouds were obtained using the proposed method. Finally, it was also quantitatively demonstrated that the GNG method overperformed other related filtering and downsampling techniques as VG and Uniform Sampling (US). 

Moreover, the GNG algorithm was extended and accelerated in order to obtain a more efficient version, suitable for operations with time constraints. It was implemented using the GPU as the main processor to compute the learning stage. As demonstrated in the experiments, the runtime of sequential GNG algorithm grew with the number of neurons as the network increased. In contrast, in the parallel version implemented on the GPU, as we increased the number of neurons, we obtained a higher acceleration over the sequential version. Experimental results showed that the GPU implementation significantly reduces learning time compared with single-threaded and multi-threaded CPU implementations of the GNG algorithm. In addition, we proposed an hybrid implementation to achieve even a higher acceleration factor of the GNG algorithm. We carried out the computation of the algorithm on the CPU during the first iterations, and then started processing data on the GPU only when there was an acceleration regarding the CPU implementation, thus achieving a higher overall acceleration of the algorithm. 

%However, although the runtime of the GNG algorithm was considerably improved, it remained still considerably high for considering it in real-time applications, as the complete learning of the map took various seconds to complete its learning for a number of neurons larger than $200$. 

The proposed 3D GNG-based method was validated in several cases of study, demonstrating its capabilities to deal with problems from different areas: mobile robotics, computer vision and CAD/CAM. Small changes were applied to the proposed method in order to solve the presented problems. Specifically, the cases of study presented a common requirement: noise removal, topology preservation and time constraints. Moreover, the reduced representation created by the GNG method not only improved later post-processing steps but also computing performance obtaining considerably accelerated runtimes. The geometry of sampled objects was preserved with a small error (input space adaptation) and without the necessity of establishing topological constraints during the learning process.

Moreover, taking advantage of the GPGPU paradigm, we proposed a real-time implementation of a 3D descriptor. A local shape 3D descriptor based on the extraction of surface patches was implemented on the GPU achieving real-time processing rates. Indeed, we also integrated on the GPU the preprocessing 3D data algorithms which were necessary for the descriptor computation. Results demonstrated that 3D processing algorithms were considerably accelerated compared to CPU implementations. Within the 3D data algorithms used in the proposed pipeline for the descriptor computation, some progresses have been made towards faster implementations of point cloud acquisition, normal estimation and point cloud triangulation algorithms.

The entire process to extract descriptors on real scenes was accelerated achieving a runtime lower than 1 sec. In fact, the implemented prototype of the proposed descriptor was applied to a object recognition application extracting $200$ tensors in less than $0.5$ seconds and successfully recognizing objects in the scene.

\section{Contributions of the Thesis}
\label{cap:conclusions:contributions}

The contributions made in this body of research are as follows:

\begin{itemize}
	\item A new method to create compact, reduced and efficient 3D representations from noisy data. The method is based on the Growing Neural Gas algorithm, which has been extended and redesigned providing several useful capabilities.
	\begin{itemize}
	\item The application of the GNG-based algorithm to represent three-dimensional noisy observations obtained with different sensors. 
	\item The extension of the original GNG algorithm to create full 3D coloured meshes captured using low-cost sensors, including colour and geometric properties.
	\item The integration of the proposed method in 3D keypoint detection algorithms. The performance of the detectors was improved using as an input data the reduced and compact representations generated by the GNG method instead of the raw point cloud.
	\item The application of the GPU as a general purpose processor to accelerate the learning process of the GNG and NG algorithms. These methods have been redesigned and optimized in order to achieve the best speed-up. Several analyses have been performed demonstrating which  stages have to be parallelized and how to fit them onto a GPU architecture.
	\item An hybrid implementation of the GNG algorithm that takes advantage of the CPU and the GPU processors in order to achieve the best performance.
	\end{itemize}
	\item The integration of 3D data processing algorithms in complex computer vision systems. Experiments have demonstrated that the GPGPU paradigm allows to considerably accelerate algorithms compared to CPU implementations and to run these in real-time.
	\begin{itemize}
	\item Normal estimation has been ported to the GPU considerably decreasing its runtime. GPU computation allowed its integration in real-time applications.
	\item Point cloud triangulation, that takes advantage of the point cloud organization, has been also ported to the GPU accelerating its runtime and allowing its execution also in real-time applications.
	\item A GPU real-time implementation of a 3D semi-local surface patch extraction algorithm.
	\end{itemize}
	\item An extensive study of the application of GNG-based methods in various computer vision applications.
	\begin{itemize}
		\item The integration of the GNG representation to improve 6DoF pose registration in mobile robotics applications.
		\item The proposed real-time implementation of 3D data processing algorithm along with the presented real-time semi-local surface patch descriptor have been applied to a 3D object recognition system that works under cluttered conditions.
		\item The GNG representation has been used in CAD/CAM applications improving the accuracy of reconstructed models. Moreover, it has also been successfully used in the 3D reconstruction of captured objects using low-cost 3D sensors.
	\end{itemize}
\end{itemize}

\section{Publications}
\label{cap:conclusions:publications}

The following articles were published as a result of the research carried out by the doctoral candidate:

\begin{itemize}
    \item Published articles in scientific journals:
		\begin{itemize}
			\item Sergio Orts-Escolano, Vicente Morell, Jose Garcia-Rodriguez, Miguel Cazorla, Robert B Fisher: \textbf{Real-time 3D semi-local surface patch extraction using GPGPU.} Journal of Real-Time Image Processing (2014). (Accepted). Impact Factor (JCR 2012): 1.156.
			\newline
		    \item Antonio Jimeno-Morenilla, Jose García-Rodriguez, Sergio Orts-Escolano, Miguel Davia-Aracil: \textbf{3D-based reconstruction using growing neural gas landmark: application to rapid prototyping in shoe last manufacturing}: The International Journal of Advanced Manufacturing Technology: May 2013. Vol 69. 657-668. Impact Factor (JCR 2012): 1.205.
		    \newline
		    \item Sergio Orts, José García Rodríguez, Diego Viejo, Miguel Cazorla, Vicente Morell: \textbf{GPGPU implementation of growing neural gas: Application to 3D scene reconstruction}. J. Parallel Distrib. Comput. 72(10): 1361-1372 (2012). Impact Factor (JCR 2011): 1.135.
		    \newline
		    \item José García Rodríguez, Anastassia Angelopoulou, Juan Manuel García Chamizo, Alexandra Psarrou, Sergio Orts-Escolano, Vicente Morell-Giménez: \textbf{Autonomous Growing Neural Gas for applications with time constraint: Optimal parameter estimation}. Neural Networks 32: 196-208 (2012). Impact Factor (JCR 2012): 1.927.
		\end{itemize}
		\clearpage
    \item International conferences:
    	\begin{itemize}
    		\item Bas Boom, Sergio Orts-Escolano, Xi Ning, Steven McDonagh, Peter Sandilands, Robert Fisher: \textbf{Point Light Source Estimation based on Scenes Recorded by a RGB-D camera.} British Machine Vision Conference, BMVC 2013, Bristol, UK.
    		\newline
    		\item Sergio Orts-Escolano, Vicente Morell, Jose Garcia-Rodriguez and Miguel Cazorla: \textbf{Point Cloud Data Filtering and Downsampling using Growing Neural Gas.} International Joint Conference on Neural Networks, IJCNN 2013, Dallas, Texas.
    		\newline
    		\item Anastassia Angelopoulou, José García Rodríguez, Alexandra Psarrou, Markos Mentzelopoulos, Bharat Reddy, Sergio Orts-Escolano, Jose Antonio Serra: \textbf{Natural User Interfaces in Volume Visualisation Using Microsoft Kinect.} International Conference on Image Analysis and Processing, ICIAP 2013, Naples, Italy: 11-19.
    		\newline
    		\item Horacio Perez-Sanchez, Gines D. Guerrero, Jose M. Garcia, Jorge Pena, Jose M. Cecilia, Gaspar Cano, Sergio Orts-Escolano and Jose Garcia-Rodriguez: \textbf{Improving Drug Discovery using a neural networks based parallel scoring functions.} International Joint Conference on Neural Networks, IJCNN 2013, Dallas, Texas.
    		\newline
    		\item Jose Antonio Serra-Perez, Jose Garcia-Rodriguez, Sergio Orts-Escolano, Juan Manuel Garcia-Chamizo, Anastassia Angelopoulou, Alexandra Psarrou, Markos Mentzeopoulos, Javier Montoyo Bojo: \textbf{3D Gesture Recognition with Growing Neural Gas.} International Joint Conference on Neural Networks, IJCNN 2013, Dallas, Texas.
			\newline
			\item José García Rodríguez, Miguel Cazorla, Sergio Orts-Escolano, Vicente Morell: \textbf{Improving 3D Keypoint Detection from Noisy Data Using Growing Neural Gas.} International Work-Conference on Artificial Neural Networks, IWANN 2013, Puerto de la Cruz, Tenerife, Spain: 480-487.
			\newline
			\item Jose Antonio Serra, José García Rodríguez, Sergio Orts-Escolano, Juan Manuel García Chamizo, Anastassia Angelopoulou, Alexandra Psarrou, Markos Mentzelopoulos, Javier Montoyo-Bojo, Enrique Domínguez: \textbf{3D Hand Pose Estimation with Neural Networks.} International Work-Conference on Artificial Neural Networks, IWANN 2013, Puerto de la Cruz, Tenerife, Spain: 504-512
			\newline
			\item Sergio Orts-Escolano, José García Rodríguez, Vicente Morell, Jorge Azorín López, Juan Manuel García Chamizo: \textbf{Multi-GPU based camera network system keeps privacy using Growing Neural Gas.} International Joint Conference on Neural Networks (IJCNN), Brisbane, Australia, June: 1-8.
			\newline
			\item Vicente Morell, Miguel Cazorla, Diego Viejo, Sergio Orts, José García Rodríguez: \textbf{A study of registration techniques for 6DoF SLAM.} International Conference of the Catalan Association for Artificial Intelligence, CCIA 2012, University of Alacant, Spain: 111-120.
			\newline
			\item José García Rodríguez, Anastassia Angelopoulou, Juan Manuel García Chamizo, Alexandra Psarrou, Sergio Orts, Vicente Morell: \textbf{Fast Autonomous Growing Neural Gas.} International Joint Conference on Neural Networks, IJCNN 2011, San Jose, California: 725-732.
			\newline
			\item José García Rodríguez, Anastassia Angelopoulou, Vicente Morell, Sergio Orts, Alexandra Psarrou, Juan Manuel García Chamizo: \textbf{Fast Image Representation with GPU-Based Growing Neural Gas.} International Work-Conference on Artificial Neural Networks, IWANN 2011, Torremolinos-Málaga, Spain: 58-65.
			\newline
			\item José García Rodríguez, Enrique Domínguez, Anastassia Angelopoulou, Alexandra Psarrou, Francisco José Mora-Gimeno, Sergio Orts, Juan Manuel García Chamizo: \textbf{Video and Image Processing with Self-Organizing Neural Networks.} International Work-Conference on Artificial Neural Networks, IWANN 2011, Torremolinos-Málaga, Spain: 98-104
    	\end{itemize}
    \item National conferences:
    	\begin{itemize}
    		\item Sergio Orts-Escolano, José García-Rodríguez, Vicente Morell-Giménez. \textbf{Procesamiento de múltiples flujos de datos con Growing Neural Gas sobre Multi-GPU.} Jornadas de Paralelismo JP, Elche, España, 2012.
    	\end{itemize}
    \item Book chapters:
    	\begin{itemize}
    		\item Vicente Morell-Gimenez, Sergio Orts-Escolano, José García Rodríguez, Miguel Cazorla, Diego Viejo. \textbf{A Review of Registration Methods on Mobile Robots.} Robotic Vision: Technologies for Machine Learning and Vision Applications. IGI GLOBAL.
    		
    		\item José García-Rodríguez, Juan Manuel García-Chamizo, Sergio Orts-Escolano, Vicente Morell-Gimenez, José Serra-Perez, Anastassa Angelolopoulou, Miguel Cazorla, Diego Viejo. \textbf{Computer Vision Applications of Self-Organizing Neural Networks.} Robotic Vision: Technologies for Machine Learning and Vision Applications. IGI GLOBAL.
    	\end{itemize}
   	\item Poster presentation:
   	    \begin{itemize}
   	    	\item Sergio Orts, Jose Garcia-Rodriguez, Diego Viejo, Miguel Cazorla, Vicente Morell, Jose Serra: \textbf{6DoF pose estimation using Growing Neural Gas 
   	 		Network.} 5th International Conference on Cognitive Systems, Cogsys 2012, TU Vienna, Austria.
   	    	\newline
   	    	\item Sergio Orts, Jose Garcia-Rodriguez, Vicente Morell. \textbf{GPU Accelerated Growing Neural Gas Network.} Programming and Tuning Massively Parallel Systems, PUMPS 2011, Barcelona, Spain. (\textbf{Honorable Mention by NVIDIA}).
   	    \end{itemize}
\end{itemize}

\section{Future Work}
\label{cap:conclusions:future}

Regarding GPU-based implementations of the GNG and NG algorithms further works will include other improvements on their acceleration: generating random patterns using the GPU and using multi-GPU computation to improve performance and to manage several neural networks learning different features of the same input space simultaneously. 

Another interesting extension of the current GPU-based implementations will be its acceleration using distributed computing. Thanks to the recent integration of the GPU into current clusters of computers, we think that a distributed implementation of our GPU-based GNG algorithm may considerable decrease the overall runtime for clustering high dimensional data. Visual exploration and analysis of multi-dimensional data have become really popular these days due to the amount of information generated on the Internet by most kind of web applications. This new area of research is also known as ''big data``.

In addition, as a recent new massive parallel architecture called Xeon Phi \cite{cramer2012openmp_xeonphi,fang2013benchmarking_xeonphi,fang2013identifying_xeon_phi} has recently be launched by Intel, we plan to port the GPU-based implementations to this new architecture. Further study and comparison on both architectures will be performed showing advantages and disadvantages of both hardware accelerators.

Furthermore, more applications of the accelerated GNG algorithm will be studied in the future.

We also plan to extend the real-time implementation of the 3D local shape descriptor. Since the current descriptor only considered geometric information and that caused in some experiments wrong results in object recognition, we plan to improve the current descriptor adding visual features extracted from the RGB information. This colour information will help solving ambiguity in cases where the geometry of the recognized objects was very similar. Moreover, the keypoint detector algorithm used for computing the proposed 3D descriptor could be improved using visual detectors on the colour information. 


 




