# PhD thesis #
## A Three-Dimensional Representation method for Noisy Point Clouds based on Growing Self-Organizing Maps accelerated on GPUs ##

This repository contains the source files of my thesis document. These source files can be compiled using PDFLaTeX, so most figures are high resolution (>300dpi) and can be download from this repository.

## Abstract ##

The research described in this thesis was motivated by the need of a robust and efficient model capable of representing 3D data obtained with 3D sensors, which are inherently noisy. In addition, time constraints were considered as these sensors are capable of providing a 3D data stream in real time. This thesis proposed the acceleration of Self-Organizing Maps (SOMs) for 3D data representation. Until now, Self-Organizing Maps have been primarily computed offline and their application in 3D data has mainly focused on noise free models, without considering time constraints. It is proposed the parallelization and optimization of the Growing Neural Gas algorithm leveraging the computing power of modern CUDA GPUs. The proposed method was applied to different problems and applications in the area of computer vision such as the recognition and localization of objects, or 3D scene reconstruction. Moreover, research was conducted on the integration of 3D data processing algorithms in complex computer vision systems. Experiments have demonstrated that the GPGPU paradigm allows to considerably accelerate algorithms compared to CPU implementations.

## Download ##

PDF version can be downloaded from the following link: [http://www.dtic.ua.es/~sorts/files/thesis.sergio.orts.escolano.pdf](http://www.dtic.ua.es/~sorts/files/thesis.sergio.orts.escolano.pdf)

Presentation slides can be downloaded from the following link: [http://www.dtic.ua.es/~sorts/files/thesis_presentation_sergio_orts-escolano_print.pdf](http://www.dtic.ua.es/~sorts/files/thesis_presentation_sergio_orts-escolano_print.pdf)